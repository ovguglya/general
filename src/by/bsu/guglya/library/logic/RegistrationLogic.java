package by.bsu.guglya.library.logic;

import by.bsu.guglya.library.database.dao.DAOException;
import by.bsu.guglya.library.database.dao.UserDAO;
/**
 * This class of registration logic
 * @author Oksana Guglya
 */
public class RegistrationLogic {
    /**
     * This static method registers user
     * @return boolean
     * @throws LogicException a LogicException
     */
    public static boolean registrateClient(String login, String password) throws LogicException {
        UserDAO userDao = UserDAO.getInstance();
        boolean result = false;
        try{
            result = userDao.registrateUser(login, password);
        }catch(DAOException ex){
            throw new LogicException(ex.getMessage());
        }
        return result;
    }
    /**
     * This static method checks login exist
     * @return boolean
     * @throws LogicException a LogicException
     */
    public static boolean checkLoginExist(String login)throws LogicException {
        UserDAO userDao = UserDAO.getInstance();
        boolean result = false;
        try{
            result = userDao.checkLoginExist(login);
        }catch(DAOException ex){
            throw new LogicException(ex.getMessage());
        }
        return result;
    }
}
