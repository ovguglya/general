package by.bsu.guglya.library.logic;

import by.bsu.guglya.library.database.dao.BookDAO;
import by.bsu.guglya.library.database.dao.DAOException;
import by.bsu.guglya.library.model.beans.*;
/**
 * This class of book logic
 * @author Oksana Guglya
 */
public class BookLogic {
    /**
     * This static method checks book exists
     * @return boolean
     * @throws LogicException a LogicException
     */
    public static int checkBookExist(String title, String author, int year, Book.TypeOfBook bookType) throws LogicException {
        BookDAO BookDao = BookDAO.getInstance();
        int result = -1;
        try {
            result = BookDao.checkBookExist(title, author, year, bookType);
        } catch (DAOException ex) {
            throw new LogicException(ex.getMessage());
        }
        return result;
    }

}
