package by.bsu.guglya.library.logic;
/**
 * Base class for logic exceptions
 * @author Oksana Guglya
 */
public class LogicException extends Exception{

    public LogicException() {
    }

    public LogicException(String message) {
        super(message);
    }

    public LogicException(String message, Throwable cause) {
        super(message, cause);
    }

    public LogicException(Throwable cause) {
        super(cause);
    }

}
