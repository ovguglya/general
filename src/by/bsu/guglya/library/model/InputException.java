package by.bsu.guglya.library.model;

/**
 * Base class for input exceptions
 * @author Oksana Guglya
 */
public class InputException extends Exception{
    public InputException(String s){
        super(s);
    }
}
