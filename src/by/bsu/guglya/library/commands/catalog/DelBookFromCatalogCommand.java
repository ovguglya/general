package by.bsu.guglya.library.commands.catalog;

import by.bsu.guglya.library.commands.Command;
import by.bsu.guglya.library.logic.CatalogLogic;
import by.bsu.guglya.library.logic.LogicException;
import by.bsu.guglya.library.managers.ConfigurationManager;
import by.bsu.guglya.library.managers.MessageManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * This class implements a pattern command
 * This class deletes book from library catalog
 * @author Oksana Guglya
 */
public class DelBookFromCatalogCommand implements Command {

    private final static String LOCALE_PARAM = "locale";
    private final static String CATALOG_ID_PARAM = "idCatalogDel";
    private final static String SUCCESS_DEL_BOOK_FROM_CATALOG_MESSAGE_ATTR = "successDelBookFromCatalog";
    private final static String UNSUCCESSFUL_DEL_BOOK_FROM_CATALOG_MESSAGE_ATTR = "unsuccessfulDelBookFromCatalog";
    private static final String DATABASE_ERROR_MESSAGE_ATTR = "errorDatabaseMessage";
    /**
     * This method deletes book from library catalog if this book does not used in the orders
     * @param request a httpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page;
        HttpSession session = request.getSession();
        String locale = (String)session.getAttribute(LOCALE_PARAM);
        MessageManager messageManager = new MessageManager(locale);
        int idCatalog = Integer.parseInt(request.getParameter(CATALOG_ID_PARAM));
        try{
            if(CatalogLogic.delCatalogItem(idCatalog)){
                String message = messageManager.getProperty(MessageManager.DEL_BOOK_FROM_CATALOG_MESSAGE);
                request.setAttribute(SUCCESS_DEL_BOOK_FROM_CATALOG_MESSAGE_ATTR, message);
            }else{
                String message = messageManager.getProperty(MessageManager.NOT_DEL_BOOK_FROM_CATALOG_MESSAGE);
                request.setAttribute(UNSUCCESSFUL_DEL_BOOK_FROM_CATALOG_MESSAGE_ATTR, message);
            }
        }catch(LogicException ex){
            request.setAttribute(DATABASE_ERROR_MESSAGE_ATTR, messageManager.getProperty(ex.getMessage()));
            page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PATH_JSP);
            return page;
        }
        page = new CatalogCommand().execute(request);
        return page;
    }

}
