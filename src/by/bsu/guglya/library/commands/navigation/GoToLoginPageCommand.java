package by.bsu.guglya.library.commands.navigation;

import by.bsu.guglya.library.commands.Command;
import by.bsu.guglya.library.managers.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
/**
 * This class implements a pattern command
 * This class forwards to login page
 * @author Oksana Guglya
 */
public class GoToLoginPageCommand implements Command {
    /**
     * This method forwards to login page
     * @param request a httpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PATH_JSP);
        return page;
    }

}
