package by.bsu.guglya.library.commands.navigation;

import by.bsu.guglya.library.commands.Command;
import by.bsu.guglya.library.managers.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
/**
 * This class implements a pattern command
 * This class forwards to registration page
 * @author Oksana Guglya
 */
public class GoToRegistrationPageCommand implements Command {
    /**
     * This method forwards to registration  page
     * @param request a httpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATION_PATH_JSP);
        return page;
    }

}
