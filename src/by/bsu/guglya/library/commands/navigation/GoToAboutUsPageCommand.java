package by.bsu.guglya.library.commands.navigation;

import by.bsu.guglya.library.commands.Command;
import by.bsu.guglya.library.managers.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
/**
 * This class implements a pattern command
 * This class forwards to about us page
 * @author Oksana Guglya
 */
public class GoToAboutUsPageCommand implements Command {
    /**
     * This method forwards to about us page
     * @param request a httpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ABOUT_US_PATH_JSP);
        return page;
    }

}

