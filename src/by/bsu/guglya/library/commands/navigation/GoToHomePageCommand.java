package by.bsu.guglya.library.commands.navigation;

import by.bsu.guglya.library.commands.Command;
import by.bsu.guglya.library.managers.ConfigurationManager;
/**
 * This class implements a pattern command
 * This class forwards to home page
 * @author Oksana Guglya
 */
import javax.servlet.http.HttpServletRequest;

public class GoToHomePageCommand implements Command {
    /**
     * This method forwards home page
     * @param request a httpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.HOME_PATH_JSP);
        return page;
    }

}
