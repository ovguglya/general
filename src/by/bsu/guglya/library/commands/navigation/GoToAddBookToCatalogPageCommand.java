package by.bsu.guglya.library.commands.navigation;

import by.bsu.guglya.library.commands.Command;
import by.bsu.guglya.library.managers.ConfigurationManager;

import javax.servlet.http.HttpServletRequest;
/**
 * This class implements a pattern command
 * This class forwards to add book to catalog page
 * @author Oksana Guglya
 */
public class GoToAddBookToCatalogPageCommand implements Command{
    /**
     * This method to add book to catalog page
     * @param request a httpServletRequest
     */
    @Override
    public String execute(HttpServletRequest request) {
        String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.BOOK_ADDITION_CATALOG_PATH_JSP);
        return page;
    }

}
