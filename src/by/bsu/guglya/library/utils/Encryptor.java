package by.bsu.guglya.library.utils;

import java.security.NoSuchAlgorithmException;

/**
 * This is interface for Encryptors
 * @author Oksana Guglya
 */
public interface Encryptor {

    public String Encrypt(String stringToEncrypt) throws NoSuchAlgorithmException;

}
