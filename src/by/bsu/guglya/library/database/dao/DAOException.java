package by.bsu.guglya.library.database.dao;
/**
 * Base class for DAO exceptions
 * @author Oksana Guglya
 */
public class DAOException extends Exception {

    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

}
