package by.bsu.guglya.library.database.dao;

import by.bsu.guglya.library.database.ConnectionPool;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This abstract class holds connection with database
 * Also contains a method of closing the connection
 * @author Oksana Guglya
 */
public abstract class AbstractDAO {
    /**
     * This is a connection
     */
    protected Connection conn;
    /**
     * This is a prepared statement
     */
    protected PreparedStatement ps;
    /**
     * This is a result set
     */
    protected ResultSet resultSet;
    /**
     * This is a logger which print some messages to log file
     */
    private static final Logger logger = Logger.getLogger(AbstractDAO.class);
    /**
     * This is a constructor
     */
    public AbstractDAO(){
    }
    /**
     * This method gets connection from connection pool
     * @throws DAOException a DAOException
     * */
    public void getConnection() throws DAOException{
        try{
            conn = ConnectionPool.getInstance().getConnection();
        }catch(SQLException ex){
            throw new DAOException(ex.getMessage());
        }
    }
    /**
     * This method closes connection, prepares statement and result set
     * @throws DAOException a DAOException
     * */
    public void closeConnection() throws DAOException{
        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException ex) {
                logger.error(ex.getMessage());
            }
        }
        if (resultSet != null) {
            try {
                resultSet.close();
            } catch (SQLException ex) {
                logger.error(ex);
            }
        }
        try{
            ConnectionPool.getInstance().returnConnection(conn);
        }catch(SQLException ex){
            throw new DAOException(ex.getMessage());
        }
    }

}
